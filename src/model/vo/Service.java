package model.vo;

/**
 * Representation of a Service object
 */
public class Service implements Comparable<Service> {
	
	private String taxi_id;
	private String trip_id;
	private int trip_seconds;
	private double trip_miles;
	private double trip_total;
	private String trip_start_timestamp;
	
	public Service(String id, String tripid, int tripseconds, double tripmiles, double triptotal, String tripstarttime){
		taxi_id=id;
		trip_id=tripid;
		trip_seconds=tripseconds;
		trip_miles=tripmiles;
		trip_total=triptotal;
		trip_start_timestamp = tripstarttime;
	}

	
	/**
	 * @return id - Trip_id
	 */
	public String getTripId() {
		return trip_id;
	}	
	
	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() {
		return taxi_id;
	}	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		return trip_seconds;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		return trip_miles;
	}
	
	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {
		return trip_total;
	}

	/**
	 * @return total - Total cost of the trip
	 */
	public String getTripStartTime() {
		return trip_start_timestamp;
	}

	@Override
	public int compareTo(Service o) {
		int res = 0;
		if(this.trip_start_timestamp.compareTo(o.trip_start_timestamp)>0)
		{
			res=1;
		}
		else if(this.trip_start_timestamp.compareTo(o.trip_start_timestamp)<0){
			res=-1;
		}
		return res;
	}
	
	public String toString(){
		return trip_start_timestamp +" - "+ taxi_id + " - " + trip_id;
	}
}
