package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import api.ITaxiTripsManager;
import model.data_structures.*;
import model.vo.*;

public class TaxiTripsManager implements ITaxiTripsManager {

	// TODO
	// Definition of data model 
	private Stack<Service> pila;
	private Queue<Service> cola;
	
	public void loadServices(String serviceFile, String taxiId) {
		// TODO Auto-generated method stub
		System.out.println("Inside loadServices with File:" + serviceFile);
		System.out.println("Inside loadServices with TaxiId:" + taxiId);
		
		pila = new Stack<Service>();
		cola = new Queue<Service>();
		
		JsonParser parser = new JsonParser();
		try{
			JsonArray arr = (JsonArray) parser.parse(new FileReader(serviceFile));
			for (int i = 0; arr != null && i < arr.size(); i++){
				JsonObject obj= (JsonObject) arr.get(i);
				if( obj.get("taxi_id") != null && obj.get("taxi_id").getAsString().equals(taxiId)){
					String id = obj.get("taxi_id").getAsString();
		
					String tripid = "Unknown";
					if ( obj.get("trip_id") != null ){
						tripid = obj.get("trip_id").getAsString(); }
		
					int tripseconds = -1;
					if ( obj.get("trip_seconds") != null ){
						tripseconds = obj.get("trip_seconds").getAsInt(); }
		
					double tripmiles = -1;
					if ( obj.get("trip_miles") != null ){
						tripmiles = obj.get("trip_miles").getAsDouble(); }
		
					double triptotal = -1;
					if ( obj.get("trip_total") != null ){ 
						triptotal = obj.get("trip_total").getAsDouble(); }
				
					String tripstarttime = "Unknown";
					if ( obj.get("trip_start_timestamp") != null ){ 
						tripstarttime = obj.get("trip_start_timestamp").getAsString();
						}
				
					Service nuevo = new Service(id,tripid, tripseconds, tripmiles, triptotal,tripstarttime);
					pila.push(nuevo);
					cola.enqueue(nuevo);
				}
			}
			if(pila.getLongitud()==0 && cola.size()==0){
				System.out.println("No se encontraron servicios con el ID especificado. Verifique el ID e intente de nuevo.\n");
				}
			else{
			System.out.println("\nSe cargaron "+pila.getLongitud()+" elementos en Pila y "+cola.size()+" elementos en Cola con el ID del taxi especificado.\n");
			}
		}
		catch (JsonIOException e1 ) {
			e1.printStackTrace();}
		catch (JsonSyntaxException e2) {
			e2.printStackTrace();}
		catch (FileNotFoundException e3) {
			e3.printStackTrace();}
	}
	
	public int [] servicesInInverseOrder() {
		Stack<Service> pTemp = pila;
		int [] resultado = new int[2];
		//El orden descendente inicia con respecto al primer elemento. Si el primer elemento es el menor de todos y es �nico, entonces solo habr�
		//un elemento ordenado, independientemente del orden de los dem�s.
		Service actual = pTemp.pop();
		resultado[0]++;
		System.out.println("Inside servicesInInverseOrder\n");
		System.out.println(actual.toString());
		while(pTemp.getLongitud()>0){
			Service siguiente = pTemp.pop();
			System.out.println(siguiente.toString());
			if(actual.compareTo(siguiente)>=0){
				resultado[0] ++;
				actual = siguiente;
			}
			else{
				resultado[1] ++;
			}
		}
		return resultado;
	}

	public int [] servicesInOrder() {
		Queue<Service> cTemp = cola;
		int [] resultado = new int[2];
		Service actual = cTemp.dequeue();
		resultado[0]++;
		System.out.println("Inside servicesInOrder");
		System.out.println(actual.toString());
		while(!cTemp.isEmpty()){
			Service siguiente = cTemp.dequeue();
			System.out.println(siguiente.toString());
			if(actual.compareTo(siguiente)<=0){
				resultado[0] ++;
				actual = siguiente;
			}
			else{
				resultado[1] ++;
			}
		}
		return resultado;
	}


}
