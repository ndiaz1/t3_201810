package model.data_structures;

public class Stack <T extends Comparable<T>> implements IStack<T> {
	
	/**
	 * Primer nodo de la pila.
	 */
	private Nodo<T> primero;
	
	/**
	 * Longitud de la pila.
	 */
	private int longitud;
	
	/**
	 * Crea una nueva pila.
	 */
	public Stack(){
		primero = null;
		longitud = 0;
	}
	
	/**
	 * Coloca el elemento dado por parámetro al inicio de la pila.
	 */
	public void push(T elemento) {
		Nodo<T> nuevo = new Nodo<T>(elemento);
		nuevo.cambiarSiguiente(primero);
		primero = nuevo;
		longitud++;
	}
	
	
	/**
	 * Elimina el primer elemento de la pila y lo retorna.
	 */
	public T pop() {
		Nodo<T> popped = null;
		if(!isEmpty()){
			popped = primero;
			primero = primero.darSiguiente();
			longitud--;
			return popped.darElemento();
		}
		else{
			return null;
		}
	}

	/**
	 * Retorna true si la pila no tiene elementos. False de lo contrario.
	 */
	public boolean isEmpty() {
		return primero==null;
	}
	
	/**
	 * Retorna la longitud de la pila.
	 * @return longitud de la pila.
	 */
	public int getLongitud(){
		return longitud;
	}
	
	/**
	 * Retorna la longitud de la pila.
	 * @return longitud de la pila.
	 */
	public Nodo<T> darPrimero(){
		return primero;
	}
	
}
