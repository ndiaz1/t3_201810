package model.data_structures;

public class Nodo<T> {

	/**
	 * Nodo siguiente del nodo actual.
	 */
	private Nodo<T> siguiente;
	
	/**
	 * Contenido del nodo actual.
	 */
	private T elemento;
	
	/**
	 * Crea un nuevo nodo con el elemento dado por parametro.
	 */
	public Nodo (T elem){
		elemento = elem;
		siguiente = null;
	}
	
	/**
	 * Retorna el elemento que est� dentro del nodo.
	 * @return elemento contenido dentro del nodo.
	 */
	public T darElemento(){
		return elemento;
	}
	
	/**
	 * Retorna el siguiente nodo del nodo actual.
	 * @return Siguiente nodo.
	 */
	public Nodo<T> darSiguiente(){
		return siguiente;
	}
	
	/**
	 * Cambia el nodo siguiente del nodo actual. 
	 * @param nuevo Nodo a asignar como siguiente.
	 */
	public void cambiarSiguiente(Nodo<T> nuevo){
		siguiente = nuevo;
	}
}
