package model.data_structures;

public class Queue<T extends Comparable<T>> implements IQueue<T> {

	/**
	 * Primer nodo de la cola.
	 */
	private Nodo<T> primero;
	
	/**
	 * Ultimo nodo de la cola.
	 */
	private Nodo<T> ultimo;
	
	/**
	 * Longitud de la cola.
	 */
	private int longitud;
	
	/**
	 * Crea una nueva cola.
	 */
	public Queue(){
		primero = null;
		ultimo = null;
		longitud = 0;
	}
	
	/**
	 * Retorna el primer nodo de la cola.
	 * @return primer nodo de la cola.
	 */
	public Nodo<T> darPrimero(){
		return primero;
	}
	
	/**
	 * Retorna el �ltimo nodo de la cola.
	 * @return �ltimo nodo de la cola.
	 */
	public Nodo<T> darUltimo(){
		return ultimo;
	}
	
	/**
	 * Agrega un nuevo elemento a la cola.
	 */
	public void enqueue(T elemento) {
		Nodo<T> nuevo = new Nodo<T>(elemento);
		if(!isEmpty()){
			ultimo.cambiarSiguiente(nuevo);
			ultimo = nuevo;
			longitud++;
		}
		else{
			primero = nuevo;
			ultimo = primero;
			longitud++;
		}
	}

	/**
	 * Elimina el primer elemento de la cola y lo retorna. Si la cola est� vac�a, retorna null.
	 */
	public T dequeue() {
		if(!isEmpty()){
		Nodo<T> dequeued = primero;
		primero = primero.darSiguiente();
		longitud--;
		return dequeued.darElemento();
		}
		else{
			return null;
		}
	}

	/**
	 * Retorna true si la cola est� vac�a (no tiene ning�n elemento). Retorna false de lo contrario.
	 */
	public boolean isEmpty() {
		return primero==null;
	}

	/**
	 * Retorna el tama�o de la cola.
	 */
	public int size() {
		return longitud;
	}

}
